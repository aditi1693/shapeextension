package examples.shapes;

import java.awt.*;
import java.io.*;

public abstract class Shape {

    private String content;


    public void saveShape(Shape shape,String filename) throws IOException {

        String content = shape.toString();

        File file = new File(filename);


//Create the file
        if (file.createNewFile())
        {
            System.out.println("File is created!");
        } else {
            System.out.println("File already exists.");
        }
//Write Content
        FileWriter writer = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(writer);

        for (String string :content.split("/")) {
            printWriter.println(string);
        }
        printWriter.close();
    }

    public  Shape loadShape(String filename) throws IOException,ShapeException {

        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        StringBuffer stringBuffer = new StringBuffer();
        int numCharsRead;
        char[] charArray = new char[1024];
        while ((numCharsRead = fileReader.read(charArray)) > 0) {
            stringBuffer.append(charArray, 0, numCharsRead);
        }
        fileReader.close();
        this.content=stringBuffer.toString();
        System.out.println(content);
        ShapeFactory shapeFactory = new ShapeFactory();
        return shapeFactory.createShape(this.content);
    }

    public abstract void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException, IOException;

    public abstract double computeArea();

    public abstract void setProperties(String s) throws ShapeException;
}
