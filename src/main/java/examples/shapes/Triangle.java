/**
 *
 */
package examples.shapes;

import java.awt.*;

/**
 * @author aditi
 *
 */
public class Triangle extends Shape {

	private  double x1;
	private  double x2;
	private  double x3;
	private  double y1;
	private  double y2;
	private  double y3;
	private Point point1;
	private Point point2;
	private Point point3;

	private Line line1;
	private Line line2;
	private Line line3;

	private double a,b,c;

	public Triangle(){}

	/**
	 * Constructor based on X-Y location

	 * @throws ShapeException   Exception throw if any parameter is invalid
	 * @throws ShapeException   Exception throw if length of any edge is smaller than 0
	 * @throws ShapeException   Exception throw if All vertices are in the same line
	 * @throws ShapeException   Exception throw if Sum of two side of triangle is not greater than third side*/



	public Triangle(double x1, double y1, double x2, double y2,double x3, double y3) throws ShapeException {

		this.x1=x1;
		this.x2=x2;
		this.x3=x3;
		this.y1=y1;
		this.y2=y2;
		this.y3=y3;

		point1= new Point(x1,y1);
		point2= new Point(x2,y2);
		point3= new Point(x3,y3);

		line1 = new Line(point1, point2);
		line2 = new Line(point2, point3);
		line3 = new Line(point3, point1);

		a=line1.computeLength();
		b=line2.computeLength();
		c=line3.computeLength();


		if ((y1 - y2) * (x1 - x3) == (y1 - y3) * (x1 - x2))
			throw new ShapeException("All vetices should not be in the same line");

	}


	/**
	 * Constructor based on points of Vertices
	 * @param point1            point of first vertices -- must not be null
	 * @param point2            point of second vertices -- must not be null
	 * @param point3            point of third vertices -- must not be null
	 * @throws ShapeException   Exception throw if any parameter is invalid
	 * @throws ShapeException   Exception throw if length of any edge is smaller than 0
	 * @throws ShapeException   Exception throw if All vertices are in the same line
	 * @throws ShapeException   Exception throw if Sum of two side of triangle is not greater than third side*/

	public Triangle(Point point1, Point point2, Point point3) throws ShapeException
	{

		if (point1==null || point2==null || point3==null )
			throw new ShapeException("Invalid Point");

		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;

		line1 = new Line(point1, point2);
		line2 = new Line(point2, point3);
		line3 = new Line(point3, point1);

		a=line1.computeLength();
		b=line2.computeLength();
		c=line3.computeLength();


		if ((point1.getY() - point2.getY()) * (point1.getX() - point3.getX()) == (point1.getY() - point3.getY()) * (point1.getX() - point2.getX()))
			throw new ShapeException("All vetices should not be in the same line");


	}

	public Triangle(double x, double y, double width, double height) throws ShapeException {
		this(new Point(x,y), new Point(x+width, y), new Point(x+(width/2), y + height));
	}

	public Triangle(Point corner, double width, double height) throws ShapeException {
		this(corner, new Point(corner.getX()+width, corner.getY()), new Point(corner.getX()+(width/2), corner.getY() + height));
	}

	public void setX1(double x1) {
		this.x1 = x1;
	}

	public void setX2(double x2) {
		this.x2 = x2;
	}

	public void setX3(double x3) {
		this.x3 = x3;
	}

	public void setY1(double y1) {
		this.y1 = y1;
	}

	public void setY2(double y2) {
		this.y2 = y2;
	}

	public void setY3(double y3) {
		this.y3 = y3;
	}

	/**
	 * @return  The first vertices
	 */
	public Point getVertices1() { return point1; }

	/**
	 * @return  The second vertices
	 */
	public Point getVertices2() { return point2; }

	/**
	 * @return  The third vertices
	 */
	public Point getVertices3() { return point3; }


	/**
	 * implementation of inherited abstract class
	 * @param deltaX            The delta x-location by which the rectangle should be moved -- must be a valid double
	 * @param deltaY            The delta y-location by which the rectangle should be moved -- must be a valid double
	 * @throws ShapeException   Exception throw if any parameter is invalid
	 */



	public void move(double deltaX, double deltaY) throws ShapeException
	{
		point1.move(deltaX, deltaY);
		point2.move(deltaX, deltaY);
		point3.move(deltaX, deltaY);
	}

	@Override
	public void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException {


		// Shift the shape by the specified rendering offset
		move(-xOffset, -yOffset);

		// Compute the left side of the bounding box
		int x = (int) Math.round(point1.getX() - line1.computeLength());

		// Compute the top side of the bounding box
		int y = (int) Math.round(point3.getY() - line3.computeLength());

		// Compute the width of the bounding box
		int width = (int) line2.computeLength();

		int [] xPoints = {(int)point1.getX(), (int)point2.getX(), (int)point3.getX()};
		int [] yPoints = {(int)point1.getY(), (int)point2.getY(), (int)point3.getY()};

		// Draw the circle by drawing an oval in a square bounding box
		graphics.drawPolygon(xPoints, yPoints, 3);

		// Shift the shape back to its original location
		move(xOffset, yOffset);

	}

	/**
	 * implementation of inherited abstract class
	 * @return  The area of the Rectangle
	 */


	public double computeArea()
	{
		double s,area;
		s = (a+b+c)/2;
		area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		return area;
	}

	@Override
	public void setProperties(String s) throws ShapeException {
		int openBr = s.indexOf('(');
		int closeBr = s.indexOf(')');
		String properties = s.substring(openBr+1,closeBr);
		String property[] = properties.split(",");
		this.setX1(Double.parseDouble(property[0]));
		this.setY1(Double.parseDouble(property[1]));
		this.setX2(Double.parseDouble(property[2]));
		this.setY2(Double.parseDouble(property[3]));
		this.setX3(Double.parseDouble(property[4]));
		this.setY3(Double.parseDouble(property[5]));

			point1= new Point(x1,y1);
			point2= new Point(x2,y2);
			point3= new Point(x3,y3);
			line1 = new Line(point1, point2);
			line2 = new Line(point2, point3);
			line3 = new Line(point3, point1);


	}

	@Override
	public String toString() {
		return  "Triangle("+getVertices1()+","+getVertices2()+","+getVertices3()+")";
	}
}
