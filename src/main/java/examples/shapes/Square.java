/**
 *
 */
package examples.shapes;

/**
 * @author aditi
 *
 */
public class Square extends Rectangle {


	public Square(){}

	/**
	 * Constructor based on x-y Locations calling constructor of Rectangle class(parent class)
	 * @param x1                The x-location of first vertices -- must be a valid double.
	 * @param y1                The y-location of first vertices -- must be a valid double.
	 * @param x2                The x-location of second vertices -- must be a valid double.
	 * @param y2                The y-location of second vertices -- must be a valid double.
	 * @param x3                The x-location of third vertices -- must be a valid double.
	 * @param y3                The y-location of third vertices -- must be a valid double.
	 * @param x4                The x-location of fourth vertices -- must be a valid double.
	 * @param y4                The y-location of fourth vertices -- must be a valid double.
	 * @throws ShapeException   Exception throw if any parameter is invalid

	 */



	public Square(double x1, double y1, double x2, double y2,double x3, double y3, double x4, double y4) throws ShapeException
	{
		super(x1, y1, x2,  y2, x3, y3, x4, y4);

		if(getLine1().computeLength()!=getLine2().computeLength() && getLine2().computeLength()!=getLine3().computeLength() && getLine3().computeLength()!=getLine4().computeLength() && getLine1().computeLength()!=getLine4().computeLength())
			throw new ShapeException("All sides of square should be equal");

	}

	public Square(double x, double y, double width, double height) throws ShapeException {
		super(x,y,width,height);
	}



	/**
	 * Constructor based on points of Vertices calling constructor of Rectangle class(parent class)
	 * @param point1            point of first vertices -- must not be null
	 * @param point2            point of second vertices -- must not be null
	 * @param point3            point of third vertices -- must not be null
	 * @param point4            point of fourth vertices -- must not be null
	 * @throws ShapeException   Exception throw if any parameter is invalid

	 */

	public Square(Point point1, Point point2, Point point3, Point point4) throws ShapeException
	{
		super(point1, point2, point3, point4);


		if(getLine1().computeLength()!=getLine2().computeLength() && getLine2().computeLength()!=getLine3().computeLength() && getLine3().computeLength()!=getLine4().computeLength() && getLine1().computeLength()!=getLine4().computeLength())
			throw new ShapeException("All sides of square should be equal");
	}

	public String toString() {
		return  "Square("+getVertices1()+","+getVertices2()+","+getVertices3()+","+getVertices4()+")";
	}

}
