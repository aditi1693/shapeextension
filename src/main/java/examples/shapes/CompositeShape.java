package examples.shapes;

import com.sun.xml.internal.ws.util.StringUtils;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class CompositeShape extends Shape {

    private ArrayList<Shape> shapeList= new ArrayList<>();
    public CompositeShape(){}


    public void addChildShape(Shape shape){
        shapeList.add(shape);
    }

    public ArrayList<Shape> getShapeList() {
        return shapeList;
    }

    public void remove(Shape shapes){
        shapeList.remove(shapes);
    }

    public void removeAll(){
        shapeList.removeAll(this.shapeList);
    }



    @Override
    public void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException, IOException {

        for (Shape s: shapeList){
            s.render(graphics,xOffset,yOffset);
        }

    }

    @Override
    public double computeArea() {
        double area=0;
        for (Shape s: shapeList){
            area+=s.computeArea();
        }
        return area;
    }

    @Override
    public void setProperties(String shapeString) throws ShapeException {

        int openBrIndex=shapeString.indexOf('(');
        int endBrIndex=shapeString.length()-1;
        shapeString=shapeString.substring(openBrIndex+1,endBrIndex);
        for(int i=0;i<shapeString.length();)
        {
            System.out.println(shapeString);
            //loop to  find firstIndex and lastIndex
            int firstIndex=shapeString.indexOf('(',i)+1;
            //now finding last bracket's index
            int firstCloseBr=shapeString.indexOf(')',i);
            String tempString=shapeString.substring(firstIndex,firstCloseBr);
            int openBrCount= tempString.length()-tempString.replace("(","").length();
            //to find endIndex go to last nth close bracket where n = openBrCount

            int count=0;
            while(count<openBrCount)
            {
                firstCloseBr=shapeString.indexOf(')',firstCloseBr+1);
                count++;
            }
            String subShapeString="";
            if(shapeString.length()==firstCloseBr+1)
            {
                subShapeString=shapeString.substring(i,firstCloseBr+1);
            }
            else
                subShapeString=shapeString.substring(i,firstCloseBr+2);
            if(shapeString.length()<=firstCloseBr+2)
            {
                break;
            }
            this.addChildShape(ShapeFactory.createShape(subShapeString));
            System.out.println(shapeString+" : "+shapeString.length()+" : "+firstCloseBr);
            if(shapeString.charAt(firstCloseBr+1)==',')shapeString=shapeString.substring(firstCloseBr+2,shapeString.length());
            else shapeString=shapeString.substring(firstCloseBr+1,shapeString.length());
            //i=firstCloseBr+2;
            System.out.println("Shape added : "+subShapeString);
        }
    }

    public String toString() {
        String shapesDetails=  "Composite(";
        Shape s=null;
        for (int i=0;i<shapeList.size();i++) {
            if(i!=0)shapesDetails+= ",";
            shapesDetails = shapesDetails + shapeList.get(i).toString();
        }
        shapesDetails=shapesDetails+")";

        return  shapesDetails;
    }
}
