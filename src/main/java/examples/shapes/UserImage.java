package examples.shapes;

import java.io.File;
import java.util.HashMap;

public class UserImage {

        private static HashMap<String, File> imageMap = new HashMap<String,File>();
        public static File manageObjects(String fileName){
            File source = imageMap.get(fileName);
            if(source == null)
            {
                source = new File(fileName);
                imageMap.put(fileName,source);
                System.out.println("added "+fileName);
            }
            return source;
        }
    }

