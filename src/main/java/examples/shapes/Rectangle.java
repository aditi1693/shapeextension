package examples.shapes;

import java.awt.*;

/**
 * @author aditi
 *
 */

public class Rectangle extends Shape {

	private double x1;
	private double x2;
	private double x3;
	private double x4;
	private double y1;
	private double y2;
	private double y3;
	private double y4;
    private Point point1;
    private Point point2;
    private Point point3;
    private Point point4;
    private Line line1;
    private Line line2;
    private Line line3;
    private Line line4;

    public Rectangle(){}
	
	  /**
     * Constructor based on x-y Locations
     * @param x1                The x-location of first vertices -- must be a valid double.
     * @param y1                The y-location of first vertices -- must be a valid double.
     * @param x2                The x-location of second vertices -- must be a valid double.
     * @param y2                The y-location of second vertices -- must be a valid double.
     * @param x3                The x-location of third vertices -- must be a valid double.
     * @param y3                The y-location of third vertices -- must be a valid double.
     * @param x4                The x-location of fourth vertices -- must be a valid double.
     * @param y4                The y-location of fourth vertices -- must be a valid double.
     * @throws ShapeException   Exception throw if any parameter is invalid
     * @throws ShapeException   Exception throw if length of any edge is smaller than 0    
     * @throws ShapeException   Exception throw if adjacent edge does not form 90 degree angle
     */
	
	  public Rectangle(double x1, double y1, double x2, double y2,double x3, double y3, double x4, double y4) throws ShapeException {

	  		this.x1=x1;
	  		this.x2=x2;
	  		this.x3=x3;
	  		this.x4=x4;
	  		this.y1=y1;
	  		this.y2=y2;
	  		this.y3=y3;
	  		this.y4=y4;
		    point1= new Point(x1,y1);
		    point2= new Point(x2,y2);
		    point3= new Point(x3,y3);
		    point4= new Point(x4,y4);
		    line1 = new Line(point1, point2);
		    line2 = new Line(point2, point3);
		    line3 = new Line(point3, point4);
		    line4 = new Line(point4, point1);
		    
		    System.out.println(line1.computeSlope());


	     if(((point1.getX()==point4.getX()) && (point2.getX()==point3.getX()) && (point1.getY()==point2.getY()) && (point3.getY()==point4.getY())) ||
		           ((point1.getX()==point2.getX()) && (point3.getX()==point4.getX()) && (point1.getY()==point4.getY()) && (point2.getY()==point3.getY())))
	     System.out.print("");
	     	else {
	            if (line1.computeSlope()*line2.computeSlope()!=-1 && line2.computeSlope()*line3.computeSlope()!=-1 && line3.computeSlope()*line4.computeSlope()!=-1 && line4.computeSlope()*line1.computeSlope()!=-1)
	     	    throw new ShapeException("All angle should be 90 Degree");
	    }
	  }
	  
	  /**
	     * Constructor based on points of Vertices
	     * @param point1            point of first vertices -- must not be null
	     * @param point2            point of second vertices -- must not be null
         * @param point3            point of third vertices -- must not be null
	     * @param point4            point of fourth vertices -- must not be null
	     * @throws ShapeException   Exception throw if any parameter is invalid
	     * @throws ShapeException   Exception throw if length of any edge is smaller than 0    
	     * @throws ShapeException   Exception throw if adjacent edge does not form 90 degree angle
	     */
	  public Rectangle(Point point1, Point point2, Point point3, Point point4) throws ShapeException {
		  
		  if (point1==null || point2==null || point3==null || point4==null)
	            throw new ShapeException("Invalid Point");
		  this.point1 = point1;
		  this.point2 = point2;
		  this.point3 = point3;
		  this.point4 = point4;

		  
		    line1 = new Line(point1, point2);
		    line2 = new Line(point2, point3);
		    line3 = new Line(point3, point4);
		    line4 = new Line(point4, point1);
		 


	        
	        if(((point1.getX()==point4.getX()) && (point2.getX()==point3.getX()) && (point1.getY()==point2.getY()) && (point3.getY()==point4.getY())) ||
	           ((point1.getX()==point2.getX()) && (point3.getX()==point4.getX()) && (point1.getY()==point4.getY()) && (point2.getY()==point3.getY())))
	            System.out.println("");

	        else {
	        
	            if (line1.computeSlope()*line2.computeSlope()!=-1 && line2.computeSlope()*line3.computeSlope()!=-1 && line3.computeSlope()*line4.computeSlope()!=-1 && line4.computeSlope()*line1.computeSlope()!=-1)
                 throw new ShapeException("All angle should be 90 Degree");
	  }
	  }

	  Rectangle(double x, double y, double width, double height) throws ShapeException {
		  point1= new Point(x,y);
		  point2= new Point(x+width,y);
		  point3= new Point(x+width,y+height);
		  point4= new Point(x,y+height);
		  line1 = new Line(point1, point2);
		  line2 = new Line(point2, point3);
		  line3 = new Line(point3, point4);
		  line4 = new Line(point4, point1);

		  System.out.println(line1.computeSlope());


	  }

	public void setX1(double x1) {
		this.x1 = x1;
	}

	public void setY1(double y1) {
		this.y1 = y1;
	}

	public void setX2(double x2) {
		this.x2 = x2;
	}

	public void setY2(double y2) {
		this.y2 = y2;
	}

	public void setX3(double x3) {
		this.x3 = x3;
	}

	public void setY3(double y3) {
		this.y3 = y3;
	}

	public void setX4(double x4) {
		this.x4 = x4;
	}

	public void setY4(double y4) {
		this.y4 = y4;
	}

	/**
	     * @return  The first vertices
	     */
	    public Point getVertices1() { return point1; }

	    /**
	     * @return  The second vertices
	     */
	    public Point getVertices2() { return point2; }
	    
		  /**
	     * @return  The third vertices
	     */
	    public Point getVertices3() { return point3; }

	    /**
	     * @return  The fourth vertices
	     */
	    public Point getVertices4() { return point4; }
	  
	    /**
	     * @return  The first Line
	     */
	    public Line getLine1() { return line1; }

	    /**
	     * @return  The second Line
	     */
	    public Line getLine2() { return line2; }
	    
		  /**
	     * @return  The third line
	     */
	    public Line getLine3() { return line3; }

	    /**
	     * @return  The fourth Line
	     */
	    public Line getLine4() { return line4; }
	  /**
	     * @return  The height of rectangle
	   */
	  
	  public double getHeight() 
	  {
		  
		  return line2.computeLength();
	  }
	  
	  /**
	     * @return  The width of rectangle
	   */
	  
	  public double getWidth()
	  {
		  
		  return line1.computeLength();
	  }

	@Override
	public void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException {



		// Compute the left side of the bounding box
		int x1 = (int) Math.round(point1.getX());

		// Compute the top side of the bounding box
		int y1 = (int) Math.round(point1.getY());

		// Compute the width of the bounding box
		int width = (int) Math.round(getWidth());

		int height = (int) Math.round(getHeight());


		// Draw the Rectangle in a square bounding box
		graphics.drawRect(x1,y1,width,height);

	}

	/**
	     * implementation of inherited abstract class 
	     * @return  The area of the Rectangle
	     */
	
	public double computeArea()
	 {
		 double height=getHeight();
		 double width=getWidth();
		 return height*width;
	 }

	@Override
	public void setProperties(String s) throws ShapeException {
		int openBr = s.indexOf('(');
		int closeBr = s.indexOf(')');
		String properties = s.substring(openBr+1,closeBr);
		String property[] = properties.split(",");
		this.setX1(Double.parseDouble(property[0]));
		this.setY1(Double.parseDouble(property[1]));
		this.setX2(Double.parseDouble(property[2]));
		this.setY2(Double.parseDouble(property[3]));
		this.setX3(Double.parseDouble(property[4]));
		this.setY3(Double.parseDouble(property[5]));
		this.setX4(Double.parseDouble(property[6]));
		this.setY4(Double.parseDouble(property[7]));
			point1= new Point(x1,y1);
			point2= new Point(x2,y2);
			point3= new Point(x3,y3);
			point4= new Point(x4,y4);
			line1 = new Line(point1, point2);
			line2 = new Line(point2, point3);
			line3 = new Line(point3, point4);
			line4 = new Line(point4, point1);

	}

	/**
	     * implementation of inherited abstract class 
	     * @param deltaX            The delta x-location by which the rectangle should be moved -- must be a valid double
	     * @param deltaY            The delta y-location by which the rectangle should be moved -- must be a valid double
	     * @throws ShapeException   Exception throw if any parameter is invalid
	     */
	     
	 
	 public void move(double deltaX, double deltaY) throws ShapeException
	 {
	        point1.move(deltaX, deltaY);
	        point2.move(deltaX, deltaY);
	        point3.move(deltaX, deltaY);
	        point4.move(deltaX, deltaY);
	 }

	@Override
	public String toString() {
		return  "Rectangle("+getVertices1()+","+getVertices2()+","+getVertices3()+","+getVertices4()+")";
	}
}
