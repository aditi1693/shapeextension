package examples.shapes;

import java.awt.*;

/**
 *
 *  Line
 *
 *  This class represents line objects that can be moved.  Users of a line can also get its length and slope.
 *
 */
//@SuppressWarnings("WeakerAccess")
public class Line extends Shape {

    private Point point1;
    private Point point2;
    private double x1;
    private double x2;
    private double y1;
    private double y2;

    public Line(){}

    /**
     * Constructor based on x-y Locations
     * @param x1                The x-location of first point -- must be a valid double.
     * @param y1                The y-location of first point -- must be a valid double.
     * @param x2                The x-location of second point -- must be a valid double.
     * @param y2                The y-location of second point -- must be a valid double.
     * @throws ShapeException   Exception throw if any parameter is invalid
     */
    public Line(double x1, double y1, double x2, double y2) throws ShapeException {
        this.x1=x1;
        this.x2=x2;
        this.y1=y1;
        this.y2=y2;
        point1 = new Point(x1, y1);
        point2 = new Point(x2, y2);

        if (computeLength() < 0.00000001)
            throw new ShapeException("A line must have a length > 0");
    }

    /**
     *
     * @param point1            The first point -- must not be null
     * @param point2            The second point -- must not b e null
     * @throws ShapeException   Exception throw if any parameter is invalid
     */
    public Line(Point point1, Point point2) throws ShapeException {
        if (point1==null || point2==null)
            throw new ShapeException("Invalid Point");

        this.point1 = point1;
        this.point2 = point2;

        if (computeLength() < 0.00000001)
            throw new ShapeException("A line must have a length > 0");
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }

    public void setY2(double y2) {
        this.y2 = y2;
    }

    /**
     * @return  The first point
     */
    public Point getPoint1() { return point1; }

    /**
     * @return  The second point
     */
    public Point getPoint2() { return point2; }

    @Override
    public void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException {
        // Shift the shape by the specified rendering offset
        move(-xOffset, -yOffset);

        // Compute the left side of the bounding box
        int x1 = (int) Math.round(point1.getX());

        // Compute the top side of the bounding box
        int y1 = (int) Math.round(point1.getY());

        int x2 = (int) Math.round(point2.getX());

        // Compute the top side of the bounding box
        int y2 = (int) Math.round(point2.getY());


        // Compute the width of the bounding box
//        int width = (int) Math.round(getW());
//
//        int height = (int) Math.round(getHeight());


        // Draw the Rectangle in a square bounding box
        graphics.drawLine(x1,y1,x2,y2);


        // Shift the shape back to its original location
        move(xOffset, yOffset);


    }

    @Override
    public double computeArea() {
        return 0;
    }

    @Override
    public void setProperties(String s) {
        int openBr = s.indexOf("(");
        int closeBr = s.indexOf(")");
        String properties = s.substring(openBr+1,closeBr);
        String property[] = properties.split(",");
        this.setX1(Double.parseDouble(property[0]));
        this.setY1(Double.parseDouble(property[1]));
        this.setX2(Double.parseDouble(property[2]));
        this.setY2(Double.parseDouble(property[3]));
    }

    /**
     * Move a line
     *
     * @param deltaX            The delta x-location by which the line should be moved -- must be a valid double
     * @param deltaY            The delta y-location by which the line should be moved -- must be a valid double
     * @throws ShapeException   Exception throw if any parameter is invalid
     */
    public void move(double deltaX, double deltaY) throws ShapeException {
        point1.move(deltaX, deltaY);
        point2.move(deltaX, deltaY);
    }

    /**
     * @return  The length of the line
     */
    public double computeLength() {
        return Math.sqrt(Math.pow(point2.getX() - point1.getX(), 2) +
                         Math.pow(point2.getY() - point1.getY(), 2));
    }

    /**
     * @return  The slope of the line
     */
    public double computeSlope() {
        return (point2.getY() - point1.getY())/(point2.getX() - point1.getX());
    }

    public String toString() {
        return  "Line("+getPoint1()+","+getPoint2()+")";
    }
}
