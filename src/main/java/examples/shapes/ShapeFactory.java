package examples.shapes;

public class ShapeFactory {
    public ShapeFactory(){}

    public static Shape createShape(String shapeString) throws ShapeException {
        String [] params= shapeString.split("\\(");
        String shapeType=params[0];
        Shape shape=getShape(shapeType);
        shape.setProperties(shapeString);
        return shape;
    }

    private static Shape getShape(String shapeType){

        System.out.println(shapeType+" created.");

        if (shapeType == null || shapeType.equals("")) {
            return null;
        }
        if (shapeType.equalsIgnoreCase("Circle")) {
            return new Circle();
        } else if (shapeType.equalsIgnoreCase("Ellipse")) {
            return new Ellipse();
        } else if (shapeType.equalsIgnoreCase("Rectangle")) {
            return new Rectangle();
        } else if (shapeType.equalsIgnoreCase("Square")) {
            return new Square();
        } else if (shapeType.equalsIgnoreCase("Triangle")) {
            return new Triangle();
        } else if (shapeType.equalsIgnoreCase("Line")) {
            return new Line();
        } else if (shapeType.equalsIgnoreCase("Composite")) {
            return new CompositeShape();
        }else if (shapeType.equalsIgnoreCase("EmbeddedPicture")) {
            return new EmbeddedPicture();
        }

        return null;
    }
}
