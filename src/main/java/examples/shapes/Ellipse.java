/**
 *
 */
package examples.shapes;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author aditi
 *
 */
public class Ellipse extends Shape {

    private double x;
    private double y;
    private Point center;
    private double a;
    private double b;

    public Ellipse() {

    }

    /**
     * Constructor with x-y Location for center
     *
     * @param x                 The x-location of the center of the circle -- must be a valid double
     * @param y                 The y-location of the center of the circle
     * @param a           The major radius of the circle -- must be greater or equal to zero.
     * @param b           The minor radius of the circle -- must be greater or equal to zero.
     * @throws ShapeException   The exception thrown if the x, y, or z are not valid
     */
    public Ellipse(double x, double y, double a, double b) throws ShapeException {
        Validator.validatePositiveDouble(a, "Invalid Majore-axis Length");
        Validator.validatePositiveDouble(b, "Invalid Minor-axis Length");

        this.x=x;
        this.y=y;
        center = new Point(x, y);
        this.a = a;
        this.b = b;
    }

    /**
     * Constructor with a Point for center
     *
     * @param center            The x-location of the center of the circle -- must be a valid point
     * @param a           The major radius of the circle -- must be greater or equal to zero.
     * @param b           The minor radius of the circle -- must be greater or equal to zero.
     * @throws ShapeException   The exception thrown if the x, y, or z are not valid
     */
    public Ellipse(Point center, double a, double b) throws ShapeException {
        Validator.validatePositiveDouble(a, "Invalid Major Radius");
        Validator.validatePositiveDouble(b, "Invalid Minor Radius");

        if (center==null)
            throw new ShapeException("Invalid center point");

        this.center = center;
        this.a = a;
        this.b = b;
    }



    /**
     * @return  The center of the circle
     */
    public Point getCenter() { return center; }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    /**
     * @return  The radius of the circle
     */
    public double getMajorRadius() { return a; }

    public double getMinorRadius() { return b; }

    public Point getFoci1()throws ShapeException
    {
        double c,x1;
        Point foci1;
        c = Math.sqrt((Math.pow(a, 2))-(Math.pow(b, 2)));
        x1 = center.getX() + c;
        foci1 = new Point(x1, center.getY());
        return foci1;

    }

    public Point getFoci2()throws ShapeException
    {
        double c,x1;
        Point foci1;
        c = Math.sqrt((Math.pow(a, 2))-(Math.pow(b, 2)));
        x1 = center.getX() - c;
        foci1 = new Point(x1, center.getY());
        return foci1;

    }




    /**
     * Move the circle
     * @param deltaX            a delta change for the x-location of center of the circle
     * @param deltaY            a delta change for the y-location of center of the circle
     * @throws ShapeException   Exception thrown if either the delta x or y are not valid doubles
     */
    public void move(double deltaX, double deltaY) throws ShapeException {
        center.move(deltaX, deltaY);
    }

    /**
     * Scale the circle
     *
     * @param scaleFactor       a non-negative double that represents the percentage to scale the circle.
     *                          0>= and <1 to shrink.
     *                          >1 to grow.
     * @throws ShapeException   Exception thrown if the scale factor is not valid
     */
    public void scale(double scaleFactor) throws ShapeException {
        Validator.validatePositiveDouble(scaleFactor, "Invalid scale factor");
        a = a*scaleFactor;
        b = b*scaleFactor;
    }

    /**
     * @return  The area of the circle.
     */
    public double computeArea() {
        return Math.PI * a * b;
    }

    @Override
    public void setProperties(String s) throws ShapeException {
        int openBr = s.indexOf('(');
        int closeBr = s.indexOf(')');
        String properties = s.substring(openBr+1,closeBr);
        String property[] = properties.split(",");
        this.setX(Double.parseDouble(property[0]));
        this.setY(Double.parseDouble(property[1]));
        this.setA(Double.parseDouble(property[2]));
        this.setB(Double.parseDouble(property[3]));
        center= new Point(x,y);
    }

    @Override
    public void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException, IOException {

        // Shift the shape by the specified rendering offset
        move(-xOffset, -yOffset);

        // Compute the left side of the bounding box
        int x = (int) Math.round(center.getX() - getMajorRadius());

        // Compute the top side of the bounding box
        int y = (int) Math.round(center.getY() - getMajorRadius());

        // Compute the width of the bounding box
        int a = (int) Math.round(getMajorRadius()*2);

        int b = (int) Math.round(getMinorRadius()*2);


        // Draw the circle by drawing an oval in a square bounding box
        graphics.drawOval(x, y, a, b);

        // Shift the shape back to its original location
        move(xOffset, yOffset);
//        BufferedImage bImg = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
//        graphics.setColor(Color.WHITE);
//        graphics.fillRect(0, 0, 100, 100);
//        graphics.setColor(Color.BLACK);
//        ImageIO.write(bImg, "png", new File("circle.png"));
    }


    public String toString() {
        return  "Ellipse("+getCenter()+","+getMajorRadius()+","+getMinorRadius()+")";
    }


}
