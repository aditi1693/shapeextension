package examples.shapes;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class EmbeddedPicture extends Shape {

    private double x;
    private double y;
    private BufferedImage img;
    private Point imageLocation;
    private String filename;
    private double height;
    private double width;
    private UserImage userImage;

    public EmbeddedPicture() {
    }

    public EmbeddedPicture(double height, double width, double x, double y, String filename) throws ShapeException {
        this.height = height;
        this.width = width;
        imageLocation = new Point(x, y);
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }


    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double computeArea() {
        return height * width;
    }

    @Override
    public void setProperties(String s) {
        int openBr = s.indexOf('(');
        int closeBr = s.indexOf(')');
        String properties = s.substring(openBr + 1, closeBr);
        String property[] = properties.split(",");
        this.setX(Double.parseDouble(property[0]));
        this.setY(Double.parseDouble(property[1]));
        this.height = (Double.parseDouble(property[2]));
        this.width = (Double.parseDouble(property[3]));
        this.filename = property[4];


        try {
            imageLocation = new Point(x, y);
        } catch (Exception e) {
        }
    }


    public void move(double deltaX, double deltaY) throws ShapeException {
        imageLocation.move(deltaX, deltaY);
    }


    @Override
    public void render(Graphics graphics, int xOffset, int yOffset) throws ShapeException, IOException {
        // Shift the shape by the specified rendering offset
        move(-xOffset, -yOffset);

        // Compute the left side of the bounding box
        int x = (int) Math.round(imageLocation.getX());

        // Compute the top side of the bounding box
        int y = (int) Math.round(imageLocation.getY());


        File sourceFile = userImage.manageObjects(filename);
        BufferedImage outputImage = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_RGB);

            outputImage = ImageIO.read(sourceFile);

        graphics.drawImage(outputImage, x, y, (int) width, (int) height, null);
        // Shift the shape back to its original location
        move(xOffset, yOffset);
    }




        @Override
        public String toString() {
            return "EmbeddedPicture(" + imageLocation.getX() + "," + imageLocation.getY() + "," + String.valueOf(height) + "," + String.valueOf(width) + "," + filename + ")";
        }
    }
