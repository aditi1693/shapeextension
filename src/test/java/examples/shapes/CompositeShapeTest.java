package examples.shapes;

import org.junit.Assert;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class CompositeShapeTest {

    @Test
    public void testAddChildShape() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void TestRender() throws ShapeException, IOException {
        CompositeShape composite = new CompositeShape();

        composite.addChildShape(new Rectangle(10, 40, 50, 20));
        CompositeShape composite2 = new CompositeShape();
        composite2.addChildShape(new Triangle(10, 10, 80, 60));
        composite.addChildShape(new EmbeddedPicture(44, 40, 60, 50, "Dog.jpg"));
        composite.addChildShape(composite2);
        Circle circle=new Circle(40, 50, 30);
        composite.addChildShape(circle);

        BufferedImage bImg = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bImg.createGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 100, 100);
        graphics.setColor(Color.BLACK);

        // Stimulus
        composite.render(graphics, 0, 0);

        // Write to a file so the results can be compared manually
        assertTrue(ImageIO.write(bImg, "png", new File("composite.png")));

        composite.saveShape(composite, "Composite.txt");
        CompositeShape c = (CompositeShape) composite.loadShape("Composite.txt");

        ArrayList<Shape> shapes = composite2.getShapeList();
        for (Shape s : shapes) {
            assertEquals("Triangle(10.0,10.0,90.0,10.0,50.0,70.0)", s.toString());
        }
        assertEquals(7987.433388230814,composite.computeArea());
        ArrayList<Shape> mainShapes = composite.getShapeList();
        int perviousSize=mainShapes.size();
        composite.remove(circle);
        assertEquals(perviousSize-1,mainShapes.size());
        composite.removeAll();
        assertEquals(0,composite.getShapeList().size());




        // There should be a dog in the bottom right corner, partially hanging off
        // the bottom and the right.  A circle the up and to the left of his head.
        // A rectangle up and to the left of the circle.  An a large isosceles
        // triangle centered horizontally and overlapping the circle and rectangle.
    }


    //@Test
//    public String toString() {
//        return null;
//    }
}
