
package examples.shapes;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

import static org.junit.Assert.*;

    public class EmbeddedPictureTest{
        @Test
        public void renderTest() throws ShapeException {
            try {
                double length = 200;
                double width = 200;
                String source = "Cat.jpg";
                EmbeddedPicture myEmbeddedPicture = new EmbeddedPicture(length, width, 10, 10, source);
                File inputFile = UserImage.manageObjects(source);
                BufferedImage inputImage = null;
                inputImage = ImageIO.read(inputFile);
                myEmbeddedPicture.setHeight(200);
                myEmbeddedPicture.setWidth(200);
                myEmbeddedPicture.computeArea();
                // creates output image
                BufferedImage outputImage = new BufferedImage((int)myEmbeddedPicture.getWidth(),(int)myEmbeddedPicture.getHeight(), inputImage.getType());
                myEmbeddedPicture.setProperties("EmbeddedPicture(10.0,10.0,200.0,200.0,Dog.jpg)");

                // scales the input image to the output image
                Graphics2D graphics = outputImage.createGraphics();
              //  graphics.setColor(Color.BLACK);
                graphics.fillRect(0, 0, (int)myEmbeddedPicture.getWidth(), (int)myEmbeddedPicture.getHeight());
              //  graphics.setColor(Color.RED);


                // scales the input image to the output image
                myEmbeddedPicture.render(graphics, 0, 0);
                assertTrue(ImageIO.write(outputImage, "png", new File("Embedded.png")));
                assertEquals("Dog.jpg",myEmbeddedPicture.getFilename());
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        @Test
        public void saveTest() throws ShapeException{
            try{
                EmbeddedPicture myEmbeddedPicture = new EmbeddedPicture(200, 200, 10, 10, "Cat.jpg");
                myEmbeddedPicture.saveShape( myEmbeddedPicture, "Embedded.txt");
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        @Test
        public void loadTest() throws ShapeException {
            try {
                File file = new File("Embedded.txt");
                EmbeddedPicture embeddedPicture = new EmbeddedPicture();
                embeddedPicture.loadShape("Embedded.txt");
                //System.out.println("Size is : " + shape.getShapes().size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Test
        public void testManageObjects() throws ShapeException{
            try
            {
                UserImage userImage= new UserImage();
                userImage.manageObjects("Cat.jpg");
                userImage.manageObjects("Dog.jpg");
                userImage.manageObjects("Flower.jpg");
                userImage.manageObjects("Elephant.jpg");
                userImage.manageObjects("Dog.jpg");
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }