package examples.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

public class ShapeFactoryTest {

    @Test
    public void testGetShapeMethod() throws ShapeException {
        ShapeFactory.createShape("Ellipse(30.0,40.0,50.0,20.0)");
        ShapeFactory.createShape("Square(30.0,40.0,80.0,40.0,80.0,90.0,30.0,90)");
        ShapeFactory.createShape("Triangle(10.0,10.0,30.0,40.0,5.0,60.0)");
        ShapeFactory.createShape("Line(20.0,30.0,80.0,90.0)");
    }
}