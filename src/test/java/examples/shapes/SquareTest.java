/**
 * 
 */
package examples.shapes;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;


/**
 * @author aditi
 *
 */
public class SquareTest {

	/**
	 * @throws java.lang.Exception
	 */
    @Test
    public void testValidConstruction() throws Exception {
        Square square=new Square();
        square.setProperties("Square(30.0,40.0,80.0,40.0,80.0,90.0,30.0,90.0)");
    	Point p1 = new Point(-1.5,3.3);
        Point p2 = new Point(0.8,3.3);
        Point p3 = new Point(0.8,1);
        Point p4 = new Point(-1.5,1);

        Square square1= new Square(p1,p2,p3,p4);
       //Point[] vertices = { p1, p2, p3, p4};

      
        //assertArrayEquals(vertices, square1.getVertices());
        assertEquals(2.30, square1.getHeight(), 0.1);
        assertEquals(2.30, square1.getWidth(), 0.1);


        Square square2 = new Square(-11.2, 5, 4.8, 5, 4.8, -11, -11.2, -11);        
        assertEquals(-11.2, square2.getVertices1().getX(), 0);
        assertEquals(5, square2.getVertices1().getY(), 0);
        assertEquals(4.8, square2.getVertices2().getX(), 0);
        assertEquals(5, square2.getVertices2().getY(), 0);
        assertEquals(4.8, square2.getVertices3().getX(), 0);
        assertEquals(-11, square2.getVertices3().getY(), 0);
        assertEquals(-11.2, square2.getVertices4().getX(), 0);
        assertEquals(-11, square2.getVertices4().getY(), 0);
        assertEquals(16, square2.getHeight(), 0.1);
        assertEquals(16, square2.getWidth(), 0.1);
        

    }

    @Test
    public void testInvalidConstruction() throws ShapeException {

        Point p1 = new Point(1, 1);
        Point p2 = new Point(1, 3.5);
        Point p3 = new Point(5.6, 3.5);
        Point p4 = new Point(5.6, 1);

        try {
            new Square(null, p2, p3, p4);
            fail("Expected exception not thrown for when the first parameter is null");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid Point", e.getMessage());
        }

        try {
            new Square(p1, null, p3, p4);
            fail("Expected exception not thrown for when the second parameter is null");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid Point", e.getMessage());
        }

        try {
            new Square(p1, p2, null, p4);
            fail("Expected exception not thrown for when the third parameter is null");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid Point", e.getMessage());
        }


        try {
            new Square(Double.POSITIVE_INFINITY, 1, 1, 3, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }

        try {
            new Square(1, Double.POSITIVE_INFINITY, 1, 3, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }

        try {
            new Square(1, 1, Double.POSITIVE_INFINITY, 3, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }

        try {
            new Square(1, 1, 1, Double.POSITIVE_INFINITY, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }

        try {
            new Square(1, 1, 1, 3, Double.POSITIVE_INFINITY, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }

        try {
            new Square(1, 1, 1, 3, 5, Double.POSITIVE_INFINITY, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }

        try {
            new Square(1, 1, 1, 3, 5, 3, Double.POSITIVE_INFINITY, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }

        try {
            new Square(1, 1, 1, 3, 5, 3, 5, Double.POSITIVE_INFINITY);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }


        try {
            new Square(p1, p1, p2, p2);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("A line must have a length > 0", e.getMessage());
        }

        try {
            new Rectangle(1, 1, 1, 1, 3, 5, 3, 5);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("A line must have a length > 0", e.getMessage());
        }

        try {
            new Square(1, 1, 4, 3, 5, 3, 6, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("All angle should be 90 Degree", e.getMessage());
        }

        Point n1 = new Point(1, 1);
        Point n2 = new Point(4, 3);
        Point n3 = new Point(5, 3);
        Point n4 = new Point(6, 1);


        try {
            new Rectangle(n1, n2, n3, n4);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("All angle should be 90 Degree", e.getMessage());
        }


        try {
            new Square(3.2, 1.1, 3.2, 4.2, 8.3, 4.2, 8.3, 1.1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("All sides of square should be equal", e.getMessage());
        }
        Point t1 = new Point(3.2, 1.1);
        Point t2 = new Point(3.2, 4.2);
        Point t3 = new Point(8.3, 4.2);
        Point t4 = new Point(8.3, 1.1);
        try {
            new Square(t1, t2, t3, t4);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("All sides of square should be equal", e.getMessage());
        }
    }


    
    @Test
    public void testComputeArea() throws ShapeException {

    	Point p1 = new Point(-1.5,3.3);
        Point p2 = new Point(0.8,3.3);
        Point p3 = new Point(0.8,1);
        Point p4 = new Point(-1.5,1);

        Square square1= new Square(p1,p2,p3,p4);
        assertEquals(5.29, square1.computeArea(), 0.001);

        Square square2 = new Square(-11.2, 5, 4.8, 5, 4.8, -11, -11.2, -11);
        assertEquals(256, square2.computeArea(), 0.001);

    }
    
    
    @Test
    public void testMove() throws ShapeException {
        Square mySquare= new Square(2, 4, 4.3, 4, 4.3, 1.7, 2, 1.7);


        mySquare.move(3, 4);
        assertEquals(5, mySquare.getVertices1().getX(), 0);
        assertEquals(8, mySquare.getVertices1().getY(), 0);
        assertEquals(7.3, mySquare.getVertices2().getX(), 0);
        assertEquals(8, mySquare.getVertices2().getY(), 0);
        assertEquals(7.3, mySquare.getVertices3().getX(), 0);
        assertEquals(5.7, mySquare.getVertices3().getY(), 0);
        assertEquals(5, mySquare.getVertices4().getX(), 0);
        assertEquals(5.7, mySquare.getVertices4().getY(), 0);

        mySquare.move(.4321, .7654);
        assertEquals(5.4321, mySquare.getVertices1().getX(), 0);
        assertEquals(8.7654, mySquare.getVertices1().getY(), 0);
        assertEquals(7.7321, mySquare.getVertices2().getX(), 0);
        assertEquals(8.7654, mySquare.getVertices2().getY(), 0);
        assertEquals(7.7321, mySquare.getVertices3().getX(), 0);
        assertEquals(6.4654, mySquare.getVertices3().getY(), 0);
        assertEquals(5.4321, mySquare.getVertices4().getX(), 0);
        assertEquals(6.4654, mySquare.getVertices4().getY(), 0);


        mySquare.move(-0.4321, -0.7654);
        assertEquals(5, mySquare.getVertices1().getX(), 0);
        assertEquals(8, mySquare.getVertices1().getY(), 0);
        assertEquals(7.3, mySquare.getVertices2().getX(), 0);
        assertEquals(8, mySquare.getVertices2().getY(), 0);
        assertEquals(7.3, mySquare.getVertices3().getX(), 0);
        assertEquals(5.7, mySquare.getVertices3().getY(), 0);
        assertEquals(5, mySquare.getVertices4().getX(), 0);
        assertEquals(5.7, mySquare.getVertices4().getY(), 0);

    }

    @Test
    public void testRenderOfSquareWithNoOffset() throws ShapeException, IOException {
        // Setup
        Square square1  = new Square(30, 40, 80, 40, 80, 90, 30,90);
        Square square= new Square(30,40,80,40);

        BufferedImage bImg = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bImg.createGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 100, 100);
        graphics.setColor(Color.BLACK);

        // Stimulus
        square1.render(graphics,-40,-40);

        // Write observed results to a file so it can be manual compared
        assertTrue(ImageIO.write(bImg, "png", new File("square.png")));
        assertEquals("Square(30.0,40.0,80.0,40.0,80.0,90.0,30.0,90.0)",square1.toString());

    }


}
